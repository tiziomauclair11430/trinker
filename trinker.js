const { values } = require("./people");
const people = require("./people");

module.exports = {
    title: function (){
        return `
$$$$$$$$\\        $$\\           $$\\                           $$\\                               
\\__$$  __|       \\__|          $$ |                          $$ |                              
   $$ | $$$$$$\\  $$\\ $$$$$$$\\  $$ |  $$\\  $$$$$$\\   $$$$$$\\  $$ | $$$$$$\\ $$\\    $$\\  $$$$$$\\  
   $$ |$$  __$$\\ $$ |$$  __$$\\ $$ | $$  |$$  __$$\\ $$  __$$\\ $$ |$$  __$$\\\\$$\\  $$  |$$  __$$\\ 
   $$ |$$ |  \\__|$$ |$$ |  $$ |$$$$$$  / $$$$$$$$ |$$ |  \\__|$$ |$$ /  $$ |\\$$\\$$  / $$$$$$$$ |
   $$ |$$ |      $$ |$$ |  $$ |$$  _$$<  $$   ____|$$ |      $$ |$$ |  $$ | \\$$$  /  $$   ____|
   $$ |$$ |      $$ |$$ |  $$ |$$ | \\$$\\ \\$$$$$$$\\ $$ |$$\\   $$ |\\$$$$$$  |  \\$  /   \\$$$$$$$\\ 
   \\__|\\__|      \\__|\\__|  \\__|\\__|  \\__| \\_______|\\__|\\__|  \\__| \\______/    \\_/     \\_______|
================================================================================================
   `.yellow
    },

    line: function(title = "="){
        return title.padStart(48, "=").padEnd(96, "=").bgBrightYellow.black
    },

    allMale: function(p){
        let n=[];
        for(x of p){
            if(x.gender === 'Male')
            n.push(x)}
            
        return n;},

    allFemale: function(p){
        let n=[];
        for(x of p){
            if(x.gender === 'Female')
            n.push(x)}
            
        return n;
        
    },

    nbOfMale: function(p){
        let n=0;
        for(x of p){
            if(x.gender === 'Male')
            n++}
            
        return n;
    },

    nbOfFemale: function(p){
        return  p.length - this.nbOfMale(p);
        
    },

    nbOfMaleInterest: function(p){
        let i=0;
        for(x of p){
            if(x.looking_for === 'M')
            i++}
            
        return i;
    },

    nbOfFemaleInterest: function(p){
        let i=0;
        for(x of p){
            if(x.looking_for === 'F')
            i++}
            
        return i;
    },

    nbOfPeopleWith2000$: function(p){
        let nb = 0;
        for(let i of p) {
            if(parseFloat(i.income.substring(1)) > 2000) {nb++;}
        }
        return nb;
    },

    nbOfPeopleLikeDrama : function(p){
        let i=0;
        for(x of p){
            if(x.pref_movie === 'Drama')
            i++}
            
        return i;
    },

    nbOfFemaleLikeSF : function(p){
        let n = 0;
        for (let x of this.allFemale(p)) {
            if (x.pref_movie.includes("Sci-Fi")) {
                n++;

            }
        }
        return n;
    },


    nbOfPeopleLikeDocuAnd$ : function(p){
        let nb = 0;
        for(let i of p) {
            if(parseFloat(i.income.substring(1)) > 1482 && i.pref_movie.includes('Documentary'))  {nb++;}
        }
        return nb;
        
    },

    AllIdOfPeople4000$ : function(p){
        let n = []

        for(let i of p)
         
        if(parseFloat(i.income.substring(1)) > 4000){
            n.push({ID:i.id,PRENOM:i.first_name,NOM:i.last_name,SOUS:i.income});
        }
        return n;

    },

    aManWithMax$ : function(p){
        let nb ;
        let s = 0;
        for(let i of this.allMale(p)) {
            if((parseFloat(i.income.substring(1)) > s )) {
                s=(parseFloat(i.income.substring(1)))
                nb = [{NOM:i.last_name, ID:i.id}]; 
            }
        }
        return nb;
        
    },
    
    SalaireMoyen : function(p){
        let nb = [];
        const reducer = (previousValue, currentValue) => previousValue + currentValue;
        for(let i of p) {
            let doll$ = parseFloat(i.income.substring(1))
            nb.push(doll$);
        }
        return nb.reduce(reducer)/p.length;
    },

    SalaireMedian : function(p){
        let nb = [];
        
        for(let i of p){
            let doll$ = parseFloat(i.income.substring(1))
            nb.push(doll$);
            nb.sort(function(a, b){
                return a - b;
            });
        }
        if (nb.length % 2 !== 0) {
            return nb[nb.length /2];
        } else {
            return (nb[nb.length / 2] + nb[(nb.length /2) +1]) / 2.0;
        }
    },
    
    nbOfPeopleGotBrr : function(p){
        let n = 0;
        for (let x of p) {
            if (x.latitude > 0) {
                n++;

            }
        }
        return n;

    },

    nbOfPeopleGotHotAnd$ : function(p){
        let nb = [];
        let ns = 0;
        const reducer = (previousValue, currentValue) => previousValue + currentValue;
        for(let i of p) {
            if(i.latitude < 0){
            let doll$ = parseFloat(i.income.substring(1))
            nb.push(doll$);
        ns++}
        }
        return nb.reduce(reducer)/ns;
    },
    
    pythagore:function (a, b){
        return Math.sqrt(Math.pow((b.long - a.long), 2) + Math.pow(b.lat - a.lat, 2));
    },

    CawtGetCaught : function(p){
        let a = {long: 0, lat:0}
        for(let x of p) {
            if(x.last_name == 'Cawt' && x.first_name == "Bérénice") {
                a.long = x.longitude;
                a.lat = x.latitude;
            }
        }
        let gap = 999999999999.0;
        let nearest_User = [];
        for(let i of p) {
            let b = {long: i.longitude, lat: i.latitude};
            if((this.pythagore(a, b) < gap) && (this.pythagore(a, b) !== 0)){
                gap = this.pythagore(a, b);
                nearest_User = [i.last_name, i.id];
            }}

        return nearest_User;
    },

        

    

    

    RuìGetCaught : function(p){
        let a = {long: 0, lat:0}
        for(let x of p) {
            if(x.last_name == 'Brach' && x.first_name == "Ruì") {
                a.long = x.longitude;
                a.lat = x.latitude;
            }
        }
        let gap = 999999999999.0;
        let nearest_User = [];
        for(let i of p) {
            let b = {long: i.longitude, lat: i.latitude};
            if((this.pythagore(a, b) < gap) && (this.pythagore(a, b) !== 0)){
                gap = this.pythagore(a, b);
                nearest_User = [i.last_name, i.id];
            }}

        return nearest_User;

    },

    tenGuyForJosee : function(p){
        let dist = [];
        let latBoshard = 57.6938555
        let longBoshard = 11.9704401
        for(let x of p) {
            let a = latBoshard - x.latitude
            let b = longBoshard - x.longitude
            let distance_entre_eux = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
            dist.push({distance : distance_entre_eux, nom : x.last_name, id : x.id } )
            dist.sort((a, b) => a.distance - b.distance)
            }
            
            let lesdix=[]
            for (i=1; i<11; i++){
                lesdix.push(dist[i]);
            }
            return lesdix;

    },

    NameOfGuyWorkForBigBro : function(p){
        let wb = [];
        for(let x of p)
        if(x.email.includes("google")){
            wb.push({nom:x.last_name, ID:x.id})

        }
        return wb;
        
    },

    lastYearForYou : function(p){
        let year = []
        
        for(let x of p){

            year.push({ nom : x.last_name, année :(new Date(x.date_of_birth))})
            year.sort(function (a, b){return a.année - b.année;})

        }
        return year[0]

    },

    youngerman : function(p){
        let year = []
        
        for(let x of p){

            year.push({nom : x.last_name, année :(new Date(x.date_of_birth))})
            year.sort(function (a, b){return b.année - a.année;})

        }
        return year[0]

    },

   

    moyennedage : function(p){
       

    },

    splitString :function (stringToSplit, separator) {
        return stringToSplit.split(separator);
    },

    filmpop: function (p) {


        let tab=[]
     
            for( let i of p){
         tab.push(i.pref_movie.split('|'))
     }
         tab=tab.reduce(function(a, b) {
           return a.concat(b);})
     
           var occurrences = [];
     for (var i = 0, j = tab.length; i < j; i++) {
        occurrences[tab[i]] = (occurrences[tab[i]] || 0) + 1;
        occurrences.sort(function(a,b){return Object.keys(b) - Object.keys(a)})
        
     
     
     
     }return occurrences[0]
     
     
     
      },

    darkHumor : function(p){
        let tab = [];
        let homme = this.allMale
        const reducer = (previousValue, currentValue) => previousValue + currentValue;
        for (let i of p){
            
            if(i.pref_movie.includes("Film-Noir") && i.gender === "Male"){
              let date = parseFloat(i.date_of_birth )
              tab.push(2021 - date)  
            }
        }
        return tab.reduce(reducer)/tab.length


    },


    match: function(p){
        return "not implemented".red;
    }
}
